<?php
$filename = __DIR__ . '/vendor/autoload.php';
require $filename;
$api = new \Yandex\Geo\Api();
function printScript($Address, $Latitude, $Longitude)
{
	return '<script type="text/javascript">
        ymaps.ready(init);
        var myMap, 
            myPlacemark;

        function init(){ 
            myMap = new ymaps.Map("map", {
                center: ['.$Latitude.', '.$Longitude.'],
                zoom: 7
            }); 
            
            myPlacemark = new ymaps.Placemark(['.$Latitude.', '.$Longitude.'], {
                hintContent: "'.$Address.'",
                balloonContent: "'.$Address.'"
            });
            
            myMap.geoObjects.add(myPlacemark);
        }
    </script>';
}
$content = '';
if(isset($_POST['adress']) && !empty($_POST['adress']))
{
	$adress = strip_tags($_POST['adress']);
	$api->setQuery($adress);
	$api
    ->setLang(\Yandex\Geo\Api::LANG_RU) // локаль ответа
    ->load();

	$response = $api->getResponse();
	$response->getFoundCount(); // кол-во найденных адресов
	$response->getQuery(); // исходный запрос
	$response->getLatitude(); // широта для исходного запроса
	$response->getLongitude(); // долгота для исходного запроса
	
	// Список найденных точек
	$collection = $response->getList();
	$content = '<div style="width: 70%; margin: 0px auto; padding: 20px;" class="card card-default"><div class="card-heading">
    <h3 class="card-title">Для указанного Вами адреса "'.$response->getQuery().'" найдены координаты</h3>
    </div><div class="panel-body">';
	foreach ($collection as $item) {
		$Place = $item->getAddress();
		$Latitude = $item->getLatitude();
		$Longitude = $item->getLongitude();
		$url = 'index.php'. "?Place=$Place&Latitude=$Latitude&Longitude=$Longitude";
		$content .=  '<hr><div>';
		$content .=  '<p><b>Адрес</b> <a href="'.$url.'">'. $item->getAddress().'</a></p>'; // адрес
	    $content .=  '<p><b>Широта</b> '. $item->getLatitude().'</p>'; // широта
	    $content .=  '<p><b>Долгота</b> '. $item->getLongitude().'</p>'; // Долгота
	    $content .=  '</div>';
	}
	$content .= '</div></div>';
	$script = printScript('Москва', 55.76, 37.64);
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Coords</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript">
    </script>    
   <?php 
   if($_SERVER['REQUEST_METHOD'] === 'GET' && array_key_exists ('Latitude', $_GET)
   && array_key_exists ('Longitude', $_GET))
   {

   		   $script = printScript($_GET['Place'], $_GET['Latitude'], $_GET['Longitude']);
   		   echo $script;
   }
   ?>
   </head>

<body>
	<div id="map" style="width: 600px; height: 400px; margin: 40px auto;"></div>
	  <form style="width: 70%; margin: 40px auto;" method="post">
	  <div class="d-flex justify-content-center">
	    <label for="exampleInputEmail1">Введите адрес</label>
	    <div class="col-md-4"><input type="text" class="form-control" id="adress" name="adress"></div>
	    <button type="submit" class="btn btn-primary">Отправить</button>
	  </div>
	</form>
	<?php echo $content; ?>
</body>
</html>